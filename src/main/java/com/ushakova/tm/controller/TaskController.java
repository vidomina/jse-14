package com.ushakova.tm.controller;

import com.ushakova.tm.api.ITaskController;
import com.ushakova.tm.api.ITaskService;
import com.ushakova.tm.enumerated.Sort;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Task;
import com.ushakova.tm.util.TerminalUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("***Task List***");
        System.out.println("***Enter Sort: ");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks = new ArrayList<>();
        if (sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else {
            final Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void create() {
        System.out.println("***Task Create***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.add(name, description);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void clear() {
        System.out.println("***Task Clear***");
        taskService.clear();
        System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByName() {
        System.out.println("***Remove Task***\nEnter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.removeOneByName(name);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneById() {
        System.out.println("***Remove Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeOneById(id);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void removeOneByIndex() {
        System.out.println("***Remove Task***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.removeOneByIndex(index);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void findOneById() {
        System.out.println("***Show Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByIndex() {
        System.out.println("***Show Task***\nEnter Index:\"");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void findOneByName() {
        System.out.println("***Show Task***\nEnter Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findOneByName(name);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        showTaskInfo(task);
        System.out.println("\n*Ok*");
    }

    @Override
    public void updateTaskById() {
        System.out.println("***Update Task***\nEnter Id:\"");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskById(id, name, description);
        if (taskUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");

    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("***Update Task***\nEnter Index:\n");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("Enter Task Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Task Description:");
        final String description = TerminalUtil.nextLine();
        final Task taskUpdated = taskService.updateTaskByIndex(index, name, description);
        if (taskUpdated == null) {
            System.out.println("\n*Fail*");
            return;
        }
        System.out.println("\n*Ok*");
    }

    @Override
    public void startTaskById() {
        System.out.println("***Set Status \"In Progress\" to Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startTaskById(id);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("***Set Status \"In Progress\" to Task***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startTaskByIndex(index);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void startTaskByName() {
        System.out.println("***Set Status \"In Progress\" to Task***\nEnter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startTaskByName(name);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void completeTaskById() {
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskById(id);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.completeTaskByIndex(index);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void completeTaskByName() {
        System.out.println("***Set Status \"Complete\" to Task***\nEnter Task Name:");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.completeTaskByName(name);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter Name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeStatusByName(name, status);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter Id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeStatusById(id, status);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter Index:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter Status:");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = taskService.changeStatusByIndex(index, status);
        if (task == null) System.out.println("\n*Fail*");
        else System.out.println("\n*Ok*");
    }

    private void showTaskInfo(final Task task) {
        if (task == null) return;
        System.out.println("Id: " + task.getId() + "\nName: "
                + task.getName() + "\nDescription: " + task.getDescription()
                + "\nStatus: " + task.getStatus().getDisplayName());
    }

}
