package com.ushakova.tm.repository;

import com.ushakova.tm.api.ITaskRepository;
import com.ushakova.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return list;
    }

    @Override
    public List<Task> findAll(Comparator<Task> comparator) {
        final List<Task> projects = new ArrayList<>(list);
        projects.sort(comparator);
        return projects;
    }

    @Override
    public void add(final Task task) {
        list.add(task);
    }

    @Override
    public void remove(final Task task) {
        list.remove(task);
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Task removeOneByName(String name) {
        final Task task = findOneByName(name);
        if (name == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneById(final String id) {
        final Task task = findOneById(id);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task removeOneByIndex(final Integer index) {
        final Task task = findOneByIndex(index);
        if (task == null) return null;
        remove(task);
        return task;
    }

    @Override
    public Task findOneById(final String id) {
        for (final Task task : list) {
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findOneByIndex(final Integer index) {
        return list.get(index);
    }

    @Override
    public Task findOneByName(final String name) {
        for (final Task task : list) {
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

}
