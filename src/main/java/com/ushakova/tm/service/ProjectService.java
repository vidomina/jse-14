package com.ushakova.tm.service;

import com.ushakova.tm.api.IProjectRepository;
import com.ushakova.tm.api.IProjectService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public List<Project> findAll(final Comparator<Project> comparator) {
        if (comparator == null) return null;
        return projectRepository.findAll(comparator);
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        projectRepository.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null) return;
        projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null) return;
        projectRepository.remove(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public Project removeOneByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeOneByName(name);
    }

    @Override
    public Project removeOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.removeOneById(id);
    }

    @Override
    public Project removeOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.removeOneByIndex(index);
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) return null;
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0) return null;
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project findOneByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findOneByName(name);
    }

    @Override
    public Project updateProjectById(final String id, final String name, final String description) {
        if (id == null || id.isEmpty()) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateProjectByIndex(final Integer index, final String name, final String description) {
        if (index == null || index < 0) return null;
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project startProjectById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project startProjectByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Override
    public Project completeProjectById(String id) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByIndex(Integer index) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project completeProjectByName(String name) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETE);
        return project;
    }

    @Override
    public Project changeStatusByName(String name, Status status) {
        if (name == null || name.isEmpty()) return null;
        final Project project = findOneByName(name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusById(String id, Status status) {
        if (id == null || id.isEmpty()) return null;
        final Project project = findOneById(id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(Integer index, Status status) {
        if (index == null || index < 0) return null;
        final Project project = findOneByIndex(index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}
