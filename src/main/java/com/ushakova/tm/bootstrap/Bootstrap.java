package com.ushakova.tm.bootstrap;

import com.ushakova.tm.api.*;
import com.ushakova.tm.constant.ArgumentConst;
import com.ushakova.tm.constant.TerminalConst;
import com.ushakova.tm.controller.CommandController;
import com.ushakova.tm.controller.ProjectController;
import com.ushakova.tm.controller.TaskController;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.repository.CommandRepository;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.TaskRepository;
import com.ushakova.tm.service.CommandService;
import com.ushakova.tm.service.ProjectService;
import com.ushakova.tm.service.TaskService;
import com.ushakova.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private void initData() {
        projectService.add("ProjectName", "My Description").setStatus(Status.COMPLETE);
        projectService.add("_ProjectName2", "My Description2").setStatus(Status.IN_PROGRESS);
        projectService.add("AProjectName3", "My Description3").setStatus(Status.NOT_STARTED);
        taskService.add("Task1", "My Task Description").setStatus(Status.COMPLETE);
        taskService.add("_64TaskName2067", "My Description27").setStatus(Status.IN_PROGRESS);
        taskService.add("ATaskName3", "My Description3").setStatus(Status.NOT_STARTED);
    }

    public void run(final String... args) {
        initData();
        System.out.println("*******************************");
        System.out.println("*   Welcome To Task Manager   *");
        System.out.println("*******************************");
        if (parseArgs(args)) System.exit(0);

        while (true) {
            System.out.println("Enter Command:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.showHelp();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                System.out.println("***Error: Argument Not Found***");
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.TASK_REMOVE_BY_ID:
                taskController.removeOneById();
                break;
            case TerminalConst.TASK_REMOVE_BY_INDEX:
                taskController.removeOneByIndex();
                break;
            case TerminalConst.TASK_REMOVE_BY_NAME:
                taskController.removeOneByName();
                break;
            case TerminalConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_ID:
                taskController.findOneById();
                break;
            case TerminalConst.TASK_VIEW_BY_INDEX:
                taskController.findOneByIndex();
                break;
            case TerminalConst.TASK_VIEW_BY_NAME:
                taskController.findOneByName();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConst.TASK_COMPLETE_BY_NAME:
                taskController.completeTaskByName();
                break;
            case TerminalConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConst.TASK_CHANGE_BY_ID:
                taskController.changeStatusById();
                break;
            case TerminalConst.TASK_CHANGE_BY_NAME:
                taskController.changeStatusByName();
                break;
            case TerminalConst.TASK_CHANGE_BY_INDEX:
                taskController.changeStatusByIndex();
                break;
            case TerminalConst.PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_ID:
                projectController.removeOneById();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeOneByIndex();
                break;
            case TerminalConst.PROJECT_REMOVE_BY_NAME:
                projectController.removeOneByName();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.PROJECT_VIEW_BY_ID:
                projectController.findOneById();
                break;
            case TerminalConst.PROJECT_VIEW_BY_INDEX:
                projectController.findOneByIndex();
                break;
            case TerminalConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_NAME:
                projectController.completeProjectByName();
                break;
            case TerminalConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_ID:
                projectController.changeStatusById();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_NAME:
                projectController.changeStatusByName();
                break;
            case TerminalConst.PROJECT_CHANGE_BY_INDEX:
                projectController.changeStatusByIndex();
                break;
            case TerminalConst.EXIT:
                System.exit(0);
                break;
            default:
                System.out.println("***Error: Command Not Found.***");
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

}
