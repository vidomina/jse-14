package com.ushakova.tm.model;

public class Command {

    public String arg = "";

    public String name = "";

    public String description = "";

    public Command(String name, String arg, String description) {
        this.arg = arg;
        this.name = name;
        this.description = description;
    }

    public Command() {
    }

    public String getArg() {
        return arg;
    }

    public void setArg(String arg) {
        this.arg = arg;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        String result = "";
        result = name != null && !name.isEmpty() ? result + name : result;
        result = arg != null && !arg.isEmpty() ? result + " [" + arg + "] " : result;
        result = description != null && !description.isEmpty() ? result + " - " + description : result;
        return result;
    }

}
