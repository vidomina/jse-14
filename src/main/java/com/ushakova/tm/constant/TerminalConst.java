package com.ushakova.tm.constant;

public interface TerminalConst {

    String VERSION = "version";

    String ABOUT = "about";

    String HELP = "help";

    String EXIT = "exit";

    String INFO = "info";

    String TASK_CREATE = "task-create";

    String TASK_CLEAR = "task-clear";

    String TASK_LIST = "task-list";

    String PROJECT_CREATE = "project-create";

    String PROJECT_CLEAR = "project-clear";

    String PROJECT_LIST = "project-list";

    String COMMANDS = "commands";

    String ARGUMENTS = "arguments";

    // TASK

    String TASK_UPDATE_BY_INDEX = "task-update-by-index";

    String TASK_UPDATE_BY_ID = "task-update-by-id";

    String TASK_VIEW_BY_ID = "task-view-by-id";

    String TASK_VIEW_BY_NAME = "task-view-by-name";

    String TASK_VIEW_BY_INDEX = "task-view-by-index";

    String TASK_REMOVE_BY_ID = "task-remove-by-id";

    String TASK_REMOVE_BY_NAME = "task-remove-by-name";

    String TASK_REMOVE_BY_INDEX = "task-remove-by-index";

    String TASK_START_BY_ID = "start-task-by-id";

    String TASK_START_BY_INDEX = "start-task-by-index";

    String TASK_START_BY_NAME = "start-task-by-name";

    String TASK_COMPLETE_BY_ID = "complete-task-by-id";

    String TASK_COMPLETE_BY_INDEX = "complete-task-by-index";

    String TASK_COMPLETE_BY_NAME = "complete-task-by-name";

    String TASK_CHANGE_BY_NAME = "change-task-status-by-name";

    String TASK_CHANGE_BY_ID = "change-task-status-by-id";

    String TASK_CHANGE_BY_INDEX = "change-task-status-by-index";

    //PROJECT

    String PROJECT_UPDATE_BY_INDEX = "project-update-by-index";

    String PROJECT_UPDATE_BY_ID = "project-update-by-id";

    String PROJECT_VIEW_BY_NAME = "project-view-by-id";

    String PROJECT_VIEW_BY_ID = "project-view-by-id";

    String PROJECT_VIEW_BY_INDEX = "project-view-by-index";

    String PROJECT_REMOVE_BY_ID = "project-remove-by-id";

    String PROJECT_REMOVE_BY_NAME = "project-remove-by-name";

    String PROJECT_REMOVE_BY_INDEX = "project-remove-by-index";

    String PROJECT_START_BY_ID = "start-project-by-id";

    String PROJECT_START_BY_INDEX = "start-project-by-index";

    String PROJECT_START_BY_NAME = "start-project-by-name";

    String PROJECT_COMPLETE_BY_ID = "complete-project-by-id";

    String PROJECT_COMPLETE_BY_INDEX = "complete-project-by-index";

    String PROJECT_COMPLETE_BY_NAME = "complete-project-by-name";

    String PROJECT_CHANGE_BY_NAME = "change-project-status-by-name";

    String PROJECT_CHANGE_BY_ID = "change-project-status-by-id";

    String PROJECT_CHANGE_BY_INDEX = "change-project-status-by-index";

}
